package com.comic_algorithm;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;

public class MyTreeNode {
    
    private static class TreeNode {
        int data;
        TreeNode leftChild;
        TreeNode rightChild;
        
        TreeNode(int data) {
            this.data = data;
        }
    }
    
    /**
     * 构建二叉树
     * @param inputList 输入序列
     * @return 构建好的二叉树
     */
    public static TreeNode createBinaryTree(LinkedList<Integer> inputList) {
        TreeNode node = null;
        if (null == inputList || inputList.isEmpty()) {
            return null;
        }
        Integer data = inputList.removeFirst();
        if (null != data) {
            node = new TreeNode(data);
            node.leftChild = createBinaryTree(inputList);
            node.rightChild = createBinaryTree(inputList);
        }
        return node;
    }
    
    /**
     * 二叉树前序遍历
     * @param node 二叉树节点
     */
    public static void preOrderTraveral(TreeNode node) {
        if (null == node) {
            return;
        }
        System.out.println(node.data);
        preOrderTraveral(node.leftChild);
        preOrderTraveral(node.rightChild);
    }
    
    /**
     * 二叉树中序遍历
     * @param node 二叉树节点
     */
    public static void inOrderTraveral(TreeNode node) {
        if (null == node) {
            return;
        }
        inOrderTraveral(node.leftChild);
        System.out.println(node.data);
        inOrderTraveral(node.rightChild);
    }
    
    /**
     * 二叉树后序遍历
     * @param node 二叉树节点
     */
    public static void postOrderTraveral(TreeNode node) {
        if (null == node) {
            return;
        }
        postOrderTraveral(node.leftChild);
        postOrderTraveral(node.rightChild);
        System.out.println(node.data);
    }
    
    /**
     * 二叉树非递归前序遍历
     * @param root 二叉树根节点
     */
    public static void preOrderTraveralWithStack(TreeNode root) {
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode treeNode = root;
        while (null != treeNode || !stack.isEmpty()) {
            // 迭代访问节点的左孩子，并入栈
            while (null != treeNode) {
                System.out.println(treeNode.data);
                stack.push(treeNode);
                treeNode = treeNode.leftChild;
            }
            // 如果节点没有左孩子，则弹出栈顶节点，访问节点右孩子
            if (!stack.isEmpty()) {
                treeNode = stack.pop();
                treeNode = treeNode.rightChild;
            }
        }
    }
    
    public static void main(String[] args) {
        LinkedList<Integer> inputList = new LinkedList<Integer>(
            Arrays.asList(new Integer[] {3, 2, 9, null, null, 10, null, null, 8, null, 4})
        );
        TreeNode treeNode = createBinaryTree(inputList);
        System.out.println("前序遍历：");
        preOrderTraveral(treeNode);
        System.out.println("中序遍历：");
        inOrderTraveral(treeNode);
        System.out.println("后序遍历：");
        postOrderTraveral(treeNode);
        System.out.println("非递归前序遍历：");
        preOrderTraveralWithStack(treeNode);
    }
    
}
