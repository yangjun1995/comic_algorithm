package com.comic_algorithm;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MyTreeNode {

    private static class TreeNode {
        int data;
        TreeNode leftChild;
        TreeNode rightChild;

        TreeNode(int data) {
            this.data = data;
        }
    }

    /**
     * 构建二叉树
     * 
     * @param inputList 输入序列
     * @return 构建好的二叉树
     */
    public static TreeNode createBinaryTree(LinkedList<Integer> inputList) {
        TreeNode node = null;
        if (null == inputList || inputList.isEmpty()) {
            return null;
        }
        Integer data = inputList.removeFirst();
        if (null != data) {
            node = new TreeNode(data);
            node.leftChild = createBinaryTree(inputList);
            node.rightChild = createBinaryTree(inputList);
        }
        return node;
    }
    
    /**
     * 二叉树层序遍历
     * @param root 二叉树根节点
     */
    public static void levelOrderTraversal(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            System.out.println(node.data);
            if (null != node.leftChild) {
                queue.offer(node.leftChild);
            }
            if (null != node.rightChild) {
                queue.offer(node.rightChild);
            }
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> inputList = new LinkedList<Integer>(
            Arrays.asList(new Integer[] { 3, 2, 9, null, null, 10, null, null, 8, null, 4 })
        );
        TreeNode treeNode = createBinaryTree(inputList);
        System.out.println("二叉树层序遍历：");
        levelOrderTraversal(treeNode);
    }

}
