#include "myarray.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * 创建一个数组
 * @param capacity 数组容量
 * @return 返回一个struct MyArray *指针
 */
struct MyArray * MyArray_init(int capacity) {
    if (capacity <= 0) {
        fprintf(stderr, "Capacity must be greater than 0.\n");
        exit(EXIT_FAILURE);
    }
    struct MyArray * p_array = (struct MyArray *) malloc(sizeof(struct MyArray));
    if (!p_array) {
        perror("MyArray_init failure.");
        exit(EXIT_FAILURE);
    }
    int * p_data = (int *) malloc(sizeof(int) * capacity);
    if (!p_data) {
        perror("MyArray -> array init failure.");
        free(p_array);
        exit(EXIT_FAILURE);
    }
    p_array -> size = 0;
    p_array -> capacity = capacity;
    p_array -> array = p_data;
    return p_array;
}

/**
 * 数组插入元素
 * @param p_array 待插入元素的数组
 * @param element 待插入的元素
 * @param index 待插入元素的位置
 */
void MyArray_insert(struct MyArray * p_array, int element, int index) {
    //判断访问下标是否超出范围
    if (index < 0 || index > p_array -> size) {
        fprintf(stderr, "Out of the array's actual element range\n");
        MyArray_deinit(p_array);
        exit(EXIT_FAILURE);
    }
    //从右向左循环，将元素逐个向右挪1位
    int i = p_array -> size - 1;
    while (i >= index) {
        *(p_array -> array + i + 1) = *(p_array -> array + i);
        --i;
    }
    *(p_array -> array + index) = element;
    ++(p_array -> size);
}

/**
 * 输出数组
 * @param p_array 待输出元素的数组
 */
void MyArray_output(struct MyArray * p_array) {
    int i = 0;
    printf("[");
    while (i < p_array -> size) {
        printf("%d", p_array -> array[i]);
        if (i < p_array -> size - 1) {
            printf(", ");
        }
        ++i;
    }
    printf("]\n");
}

/**
 * 释放创建的数组
 * @param p_array 待释放的数组
 */
void MyArray_deinit(struct MyArray * p_array) {
    free(p_array -> array);
    p_array -> array = NULL;
    free(p_array);
    p_array = NULL;
}
