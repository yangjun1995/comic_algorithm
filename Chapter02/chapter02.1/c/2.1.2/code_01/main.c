#include "myarray.h"

int main(int argc, char * argv[]) {
    struct MyArray * p_array = MyArray_init(10);
    MyArray_insert(p_array, 3, 0);
    MyArray_insert(p_array, 7, 1);
    MyArray_insert(p_array, 9, 2);
    MyArray_insert(p_array, 5, 3);
    MyArray_insert(p_array, 6, 1);
    MyArray_output(p_array);
    MyArray_deinit(p_array);

    return 0;
}