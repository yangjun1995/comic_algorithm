#ifndef __MYARRAY_H
#define __MYARRAY_H

struct MyArray {
    int size;
    int capacity;
    int * array;
};

extern struct MyArray * MyArray_init(int capacity);

extern void MyArray_resize(struct MyArray * p_array);

extern void MyArray_insert(struct MyArray * p_array, int element, int index);

extern void MyArray_output(struct MyArray * p_array);

extern void MyArray_deinit(struct MyArray * p_array);

#endif //__MYARRAY_H