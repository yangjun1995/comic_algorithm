#include "MyLinkedList.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
    struct MyLinkedList * p_link = MyLinkedList_init();
    MyLinkedList_insert(p_link, 3, 0);
    MyLinkedList_insert(p_link, 7, 1);
    MyLinkedList_insert(p_link, 9, 2);
    MyLinkedList_insert(p_link, 5, 3);
    MyLinkedList_insert(p_link, 6, 1);
    struct Node * p_removeNode = MyLinkedList_remove(p_link, 0);
    printf("Removed Node is: %d\n", p_removeNode -> data);
    Node_deinit(p_removeNode);
    MyLinkedList_output(p_link);
    MyLinkedList_deinit(p_link);

    return 0;
}