#ifndef __MYLINKEDLIST_H
#define __MYLINKEDLIST_H

struct Node {
    int data;
    struct Node * next;
};

struct MyLinkedList {
    //头节点指针
    struct Node * head;
    //尾节点指针
    struct Node * last;
    //链表实际长度
    unsigned int size;
};

extern struct Node * Node_init(int data);

extern void Node_deinit(struct Node * p_node);

extern struct MyLinkedList * MyLinkedList_init();

extern void MyLinkedList_deinit(struct MyLinkedList * p_link);

extern struct Node * MyLinkedList_get(struct MyLinkedList * p_link, int index);

extern void MyLinkedList_insert(struct MyLinkedList * p_link, int data, int index);

extern struct Node * MyLinkedList_remove(struct MyLinkedList * p_link, int index);

extern void MyLinkedList_output(struct MyLinkedList * p_link);

#endif //__MYLINKEDLIST_H