#ifndef __MYQUEUE_H
#define __MYQUEUE_H

struct MyQueue {
    int front;
    int rear;
    int capacity;
    int * array;
};

extern struct MyQueue * MyQueue_init(const int capacity);

extern void MyQueue_enQueue(struct MyQueue * p_queue, const int element);

extern int MyQueue_deQueue(struct MyQueue * p_queue);

extern void MyQueue_output(const struct MyQueue * const p_queue);

extern void MyQueue_deinit(struct MyQueue * p_queue);

#endif //__MYQUEUE_H
