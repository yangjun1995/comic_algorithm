#include "MyQueue.h"

int main(int argc, char *argv[]) {
    struct MyQueue * p_queue = MyQueue_init(6);
    MyQueue_enQueue(p_queue, 3);
    MyQueue_enQueue(p_queue, 5);
    MyQueue_enQueue(p_queue, 6);
    MyQueue_enQueue(p_queue, 8);
    MyQueue_enQueue(p_queue, 1);
    MyQueue_deQueue(p_queue);
    MyQueue_deQueue(p_queue);
    MyQueue_deQueue(p_queue);
    MyQueue_enQueue(p_queue, 2);
    MyQueue_enQueue(p_queue, 4);
    MyQueue_enQueue(p_queue, 9);
    MyQueue_output(p_queue);
    MyQueue_deinit(p_queue);

    return 0;
}